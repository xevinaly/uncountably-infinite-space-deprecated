import seedrandom from 'seedrandom';
import { addUser, removeUser, heartbeat } from '$lib/server/games/quiddler/users.js';
import { startWebsocket } from '$lib/server/games/quiddler/websocket.js';
import { HTTP } from '$lib/constants.js';

startWebsocket();

export async function post({ request }) {
	const username = (await request.json()).username;
	const random = seedrandom(username, { entropy: true });
	const token = random();
	const status = addUser(username, token);

	if (status == HTTP.CREATED)
		return {
			status: HTTP.CREATED,
			body: {
				token: token
			}
		};
	return { status };
}

export async function put({ request }) {
	const data = (await request.json());
	const username = data.username;
	const token = data.token;

	return { status: heartbeat(username, token) };
}
