import { addLobby } from '$lib/server/games/quiddler/hub.js';
import { validate } from '$lib/server/games/quiddler/users.js';
import { HTTP } from '$lib/constants.js';

export async function post({ request }) {
	const data = (await request.json());
	const username = data.username;
	const token = data.token;
	const status = validate(username, token);
	
	if (status == HTTP.OK)
		return {
			status,
			body: {
				id: addLobby(data)
			}
		};
	return { status };
}
