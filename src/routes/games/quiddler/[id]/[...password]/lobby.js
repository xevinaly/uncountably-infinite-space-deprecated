import { validate } from '$lib/server/games/quiddler/users.js';
import * as Hub from '$lib/server/games/quiddler/hub.js';
import { startWebsocket } from '$lib/server/signaling/index.js';
import { HTTP } from '$lib/constants.js';

startWebsocket();

export function get({ params }) {
	const { id, password } = params;
	const game = Hub.getLobbyByID(id);

	if (game) {
		if (game.private && game.password != password)
			return {
				status: HTTP.FORBIDDEN
			};
		else
			return {
				body: {
					game
				}
			};
	}
}

export async function patch({ params, request }) {
	const data = (await request.json());
	const status = validate(data.username, data.token);
	const owner = Hub.validateOwner(params.id, data.username);

	if (status != HTTP.OK) return { status };

	if (!owner)
		return {
			status: HTTP.LOCKED
		};

	if (data.method == 'reset') Hub.reset(params.id);
	else if (data.method == 'remove_user')
		Hub.removeUser(params.id, data.user);
	else if (data.method == 'add_spectator')
		Hub.addSpectator(params.id, data.spectator);
	else if (data.method == 'remove_spectator')
		Hub.removeSpectator(params.id, data.user);
	else if (data.method == 'add_player')
		Hub.addPlayer(params.id, data.player);
	else if (data.method == 'remove_player')
		Hub.removePlayer(params.id, data.player);
	else if (data.method == 'ban_user') Hub.banUser(params.id, data.user);
	else if (data.method == 'unban_user')
		Hub.unbanUser(params.id, data.user);
	else if (data.method == 'start_game') Hub.startGame(params.id);
	else if (data.method == 'end_game') Hub.endGame(params.id);
	else if (data.method == 'close') Hub.removeLobby(params.id);
	else
		return {
			status: HTTP.BAD_REQUEST
		};

	return {
		status: HTTP.NO_CONTENT
	};
}

export async function del({ params, request }) {
	const data = (await request.json());
	const status = validate(data.username, data.token);
	const owner = Hub.validateOwner(params.id, data.username);

	if (status != HTTP.OK) return { status };

	if (!owner)
		return {
			status: HTTP.LOCKED
		};

	Hub.removeLobby(params.id);

	return {
		status: HTTP.NO_CONTENT
	};
}
