export function shuffle(array) {
	let index = array.length,  selected;

  while (index != 0) {
    selected = Math.floor(Math.random() * index);
    index--;

    [array[index], array[selected]] = [
      array[selected], array[index]];
  }

	return array;
}

export function modulo(n, m) {
	return ((n % m) + m) % m;
}
