import adapter from 'webrtc-adapter';
import { configuration } from '$lib/constants.js';

export function startWebRTC(socket, key) {
	let connection = new RTCPeerConnection(configuration);

	connection.onnegotiationneeded = async () => {
		try {
			let offer = await connection.createOffer();
			await connection.setLocalDescription(offer);

			socket.send(
				JSON.stringify({
					method: 'offer',
					data: {
						connection: key,
						description: offer
					}
				})
			);
		} catch (err) {
			console.log('Negotiation Error:\n' + err);
		}
	};

	connection.onicecandidate = async ({ candidate }) => {
		socket.send(
			JSON.stringify({
				method: 'candidate',
				data: {
					connection: key,
					candidate
				}
			})
		);
	};

	socket.methods.push(async response => {
		let message = JSON.parse(response.data);

		if (message.method == 'answer') {
			try {
				await connection.setRemoteDescription(message.data.description);
			} catch (err) {
				console.log('Answer Error:\n' + err);
			}
		} else if (
			message.method == 'candidate' &&
			key == message.data.connection &&
			message.data.candidate
		) {
			try {
				await connection.addIceCandidate(
					new RTCIceCandidate(message.data.candidate)
				);
			} catch (err) {
				console.log('Candidate Error:\n' + err);
			}
		}
	});

	return connection;
}
