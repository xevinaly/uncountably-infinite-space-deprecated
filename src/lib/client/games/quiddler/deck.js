const deck = [
  'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a',
  'b', 'b',
  'c', 'c',
  'd', 'd', 'd', 'd',
  'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e',
  'f', 'f',
  'g', 'g', 'g', 'g',
  'h', 'h',
  'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i',
  'j', 'j',
  'k', 'k',
  'l', 'l', 'l', 'l',
  'm', 'm',
  'n', 'n', 'n', 'n', 'n', 'n',
  'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o',
  'p', 'p',
  'q', 'q',
  'r', 'r', 'r', 'r', 'r', 'r',
  's', 's', 's', 's',
  't', 't', 't', 't', 't', 't',
  'u', 'u', 'u', 'u', 'u', 'u',
  'v', 'v',
  'w', 'w',
  'x', 'x',
  'y', 'y', 'y', 'y',
  'z', 'z',
  'er', 'er',
  'cl', 'cl',
  'in', 'in',
  'th', 'th',
  'qu', 'qu'
];

let index = 0;

export function cardsRemaining() {
  return 118 - index;
}

export function draw() {
  return deck[index++];
}

export function shuffle() {
  index = deck.length;

  while (index-- != 0) {
    let replacement = Math.floor(Math.random() * index);

    [deck[index], deck[replacement]] = [deck[replacement], deck[index]];
  }

  index = 0;
}
