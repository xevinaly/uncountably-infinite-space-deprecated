import * as deck from '$lib/client/games/quiddler/deck.js';

const channels = new Map();
const handlers = new Map();

export const engine = {
	hub: '',
	authorization: {},
	game: null,
	handle: (message, source = null) => {
		const { method, data } = message;

		if (handlers.has(method)) handlers.get(method)(data, source);
	}
};

handlers.set('reset', (data, source) => {
	fetch(engine.hub, {
		method: 'PATCH',
		body: JSON.stringify({
			method: 'reset',
			...engine.authorization
		})
	});

	broadcast(
		JSON.stringify({
			method: 'reset'
		})
	);
});

handlers.set('connect', (data, source) => {
	if (engine.game.banned.includes(data.username)) {
		source.close();
	} else if (!engine.game.players.includes(data.username)) {
		channels.set(data.username, source);

		fetch(engine.hub, {
			method: 'PATCH',
			body: JSON.stringify({
				method: 'add_spectator',
				...engine.authorization,
				spectator: data.username
			})
		});

		broadcast(
			JSON.stringify({
				method: 'add_spectator',
				data
			})
		);
	}
});

handlers.set('disconnect', (data, source) => {
	let username = getUsername(source);

	channels.delete(username);

	fetch(engine.hub, {
		method: 'PATCH',
		body: JSON.stringify({
			method: engine.game.active ? 'remove_spectator' : 'remove_user',
			...engine.authorization,
			user: username
		})
	});

	broadcast(
		JSON.stringify({
			method: engine.game.active ? 'remove_spectator' : 'remove_user',
			data: {
				username
			}
		})
	);
});

handlers.set('add_player', (data, source) => {
	if (engine.game.max_players > engine.game.players.length) {
		fetch(engine.hub, {
			method: 'PATCH',
			body: JSON.stringify({
				method: 'add_player',
				...engine.authorization,
				player: data.username
			})
		});

		broadcast(
			JSON.stringify({
				method: 'add_player',
				data
			})
		);
	}
});

handlers.set('remove_player', (data, source) => {
	fetch(engine.hub, {
		method: 'PATCH',
		body: JSON.stringify({
			method: 'remove_player',
			...engine.authorization,
			player: data.username
		})
	});

	broadcast(
		JSON.stringify({
			method: 'remove_player',
			data
		})
	);
});

handlers.set('ban_user', (data, source) => {
	fetch(engine.hub, {
		method: 'PATCH',
		body: JSON.stringify({
			method: 'ban_user',
			...engine.authorization,
			user: data.username
		})
	});

	broadcast(
		JSON.stringify({
			method: 'ban_user',
			data
		})
	);
});

handlers.set('unban_user', (data, source) => {
	fetch(engine.hub, {
		method: 'PATCH',
		body: JSON.stringify({
			method: 'unban_user',
			...engine.authorization,
			user: data.username
		})
	});

	broadcast(
		JSON.stringify({
			method: 'unban_user',
			data
		})
	);
});

handlers.set('start_game', (data, source) => {
	fetch(engine.hub, {
		method: 'PATCH',
		body: JSON.stringify({
			method: 'start_game',
			...engine.authorization
		})
	});

	broadcast(
		JSON.stringify({
			method: 'start_game'
		})
	);
});

handlers.set('end_game', (data, source) => {
	fetch(engine.hub, {
		method: 'PATCH',
		body: JSON.stringify({
			method: 'end_game',
			...engine.authorization
		})
	});

	broadcast(
		JSON.stringify({
			method: 'end_game'
		})
	);
});

handlers.set('message', (data, source) => {
	broadcast(
		JSON.stringify({
			method: 'message',
			data
		})
	);
});

handlers.set('close', (data, source) => {
	fetch(engine.hub, {
		method: 'DELETE',
		body: JSON.stringify({
			...engine.authorization
		})
	});

	broadcast(
		JSON.stringify({
			method: 'close'
		})
	);

	channels.forEach(channel => channel.close());
});

function broadcast(message) {
	channels.forEach(channel => channel.send(message));

	engine.update({ data: message });
}

function getUsername(source) {
  for (let [username, channel] of channels.entries())
    if (channel === source)
      return username;
}
