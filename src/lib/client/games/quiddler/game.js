import { writable } from 'svelte/store';

function loadGame() {
	if (typeof window !== 'undefined') {
		const { subscribe, set, update } = writable({});
		const handlers = new Map();

		handlers.set('reset', data => {
			update(n => {
				if (!n.active) n.players = [n.owner];
				n.spectators = [];
				return n;
			});
		});

		handlers.set('remove_user', data => {
			update(n => {
				n.players = n.players.filter(player => player != data.username);
				n.spectators = n.spectators.filter(
					spectator => spectator != data.username
				);
				return n;
			});
		});

		handlers.set('add_spectator', data => {
			update(n => {
				if (!n.spectators.includes(data.username))
					n.spectators = [...n.spectators, data.username];
				n.players = n.players.filter(player => player != data.username);
				return n;
			});
		});

		handlers.set('remove_spectator', data => {
			update(n => {
				n.spectators = n.spectators.filter(
					spectator => spectator != data.username
				);
				return n;
			});
		});

		handlers.set('add_player', data => {
			update(n => {
				if (!n.players.includes(data.username))
					n.players = [...n.players, data.username];
				n.spectators = n.spectators.filter(
					spectator => spectator != data.username
				);
				return n;
			});
		});

		handlers.set('remove_player', data => {
			update(n => {
				n.players = n.players.filter(player => player != data.username);
				if (!n.spectators.includes(data.username))
					n.spectators = [...n.spectators, data.username];
				return n;
			});
		});

		handlers.set('ban_user', data => {
			update(n => {
				n.players = n.players.filter(player => player != data.username);
				if (!n.spectators.includes(data.username))
					n.spectators = [...n.spectators, data.username];
				if (!n.banned.includes(data.username))
					n.banned = [...n.banned, data.username];
				return n;
			});
		});

		handlers.set('unban_user', data => {
			update(n => {
				n.banned = n.banned.filter(user => user != data.username);
				return n;
			});
		});

		handlers.set('start_game', data => {
			update(n => {
				n.active = true;
				return n;
			});
		});

		handlers.set('end_game', data => {
			update(n => {
				n.active = false;
				return n;
			});
		});

		handlers.set('message', data => {
			update(n => {
				n.chat.messages.push(data);
				if (!n.chat.open) n.chat.viewed = false;
				return n;
			});
		});

		handlers.set('close', data => {
			set(undefined);
		});

		return {
			subscribe,
			set,
			update,
			protocol: 'quiddler',
			initialize: data => {
				set({
					...data,
					chat: {
						viewed: true,
						open: false,
						messages: []
					},
					hand: [],
					discard: 'a',
					announcement: '',
					closed: false,
					connected: true
				});
			},
			play: () => {},
			handle: message => {
				const { method, data } = JSON.parse(message.data);

				if (handlers.has(method)) handlers.get(method)(data);
			}
		};
	}
}

export const game = loadGame();
