import WebSocket from 'isomorphic-ws';
import { writable } from 'svelte/store';
import { TIME, WEBSOCKET } from '$lib/constants.js';

export function loadHub(url, username, token) {
	const { subscribe, set, update } = writable([]);

	let socket = new WebSocket(url);

	socket.onmessage = response => {
		let message = JSON.parse(response.data);

		if (message.method == 'connect') set(message.data);
		else if (message.method == 'open') update(n => [...n, message.data]);
		else if (message.method == 'close')
			update(n => n.filter(lobby => lobby.id != message.data.id));
		else if (message.method == 'update')
			update(n =>
				n.map(lobby => (lobby.id == message.data.id ? message.data : lobby))
			);
	};

	let heartbeat = setInterval(() => {
		socket.send('Heartbeat');
	}, TIME.TWENTY_SECONDS);

	return {
		subscribe,
		set,
		close: () => {
			clearInterval(heartbeat);
			socket.close(WEBSOCKET.NORMAL_CLOSURE_CODE);
			return null;
		}
	};
}
