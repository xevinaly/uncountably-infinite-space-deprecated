import { writable } from 'svelte/store';
import { PIECES } from './constants.js';
import { shuffle } from '$lib/util.js';
import EventDie from '$lib/components/games/runes-and-ruins/dice/EventDie.svelte';
import WeaponDie from '$lib/components/games/runes-and-ruins/dice/WeaponDie.svelte';
import BlacksmithDie from '$lib/components/games/runes-and-ruins/dice/BlacksmithDie.svelte';
import LootDie from '$lib/components/games/runes-and-ruins/dice/LootDie.svelte';
import BurglarDie from '$lib/components/games/runes-and-ruins/dice/BurglarDie.svelte';

let index = 0;

function loadGame() {
	const { subscribe, set, update } = writable({
		pieces: {
			collected: 0,
			mask: 0b0,
			order: [
				...shuffle([PIECES.ONE, PIECES.TWO, PIECES.THREE, PIECES.FOUR, PIECES.FIVE]),
				PIECES.SIX
			]
		},
    dice: [
      generateDie(LootDie)
    ]
  });

	return {
		subscribe,
		set,
		update
	};
}

function generateDie(type) {
	return {
		id: index++,
		type: type
	};
}

export const game = loadGame();
