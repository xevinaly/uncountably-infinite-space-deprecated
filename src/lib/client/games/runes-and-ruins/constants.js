export const PIECES = Object.freeze({
	ONE: 0b1,
  TWO: 0b10,
  THREE: 0b100,
  FOUR: 0b1000,
  FIVE: 0b10000,
  SIX: 0b100000,
	COMPLETE: 0b111111
});

export const CLASSES = Object.freeze({
	PEASANT: Object.freeze({
    id: 0b1,
    hidden: false,
    name: 'Peasant',
    description: 'A poor farmer down on their luck that came to the City seeking fortune.',
    special: 'None',
    condition: 'None'
	}),
	SORCERER: Object.freeze({
    id: 0b10,
    hidden: true,
    name: 'Sorcerer',
    description: 'A scholar of the arcane arts hoping to reforge the Great Sorcer\'s amulet and claim its power for themself.',
    special: 'All spells cost 1 Rune less to cast',
    condition: 'Beat the game with a completed amulet.'
	}),
  MESMER: Object.freeze({
    id: 0b100,
    hidden: false,
    name: 'Mesmer',
    description: 'A practitioner of an art older than magic that uses spices and spells to compell other\'s to do your bidding.',
    special: 'Can play Monsters on a Monsters, killing both',
    condition: 'Have a Monster follow you through six rooms.'
	}),
  BURGLAR: Object.freeze({
    id: 0b1000,
    hidden: false,
    name: 'Burglar',
    description: 'A common theif from the City hoping to find a score that will let them retire.',
    special: 'Can unlock a Chest without a Key for reduced rewards',
    condition: 'Unlock two Chests in one turn.'
	}),
  BLACKSMITH: Object.freeze({
    id: 0b10000,
    hidden: false,
    name: 'Blacksmith',
    description: 'A creator of weapon\'s who chose to take up the blade to avenge a relative swallowed by The Ruin.',
    special: 'Weapon shatter chances are inverted',
    condition: 'Have a Weapon survive three uses'
	}),
  LEGEND: Object.freeze({
    id: 0b100000,
    hidden: true,
    name: 'Legend',
    description: 'A hero whose name is known far and wide who has pledged to clear the dungeon and resolve it\'s threat once and for all.',
    special: 'Monsters become Chests when killed',
    condition: 'Beat the game with the Legend status'
	}),
  ACROBAT: Object.freeze({
    id: 0b1000000,
    hidden: true,
    name: 'Acrobat',
    description: 'With a reaction speed trained since young on the trapeez, they delved into The Ruin seeking thrills.',
    special: 'Does not take damage from Traps',
    condition: 'Die to a Trap'
	}),
  ILLUSIONIST: Object.freeze({
    id: 0b10000000,
    hidden: false,
    name: 'Illusionist',
    description: 'A lesser mage specialized in tricking the senses and moving unseen.',
    special: 'Monsters do not follow you to the next room',
    condition: 'Use Shimmer to avoid five damage in a single turn'
	}),
  ALCHEMIST: Object.freeze({
    id: 0b100000000,
    hidden: true,
    name: 'Alchemist',
    description: 'A seeker of the Philosopher\' Stone who believes the Great Sorcerer\'s amulet may hold a clue to it\'s creation.',
    special: 'Transmute has no cost',
    condition: 'Trasmute one Key or Weapon twice'
	}),
  VAMPIRE: Object.freeze({
    id: 0b1000000000,
    hidden: true,
    name: 'Vampire',
    description: 'A mythical denizen of the night hoping to steal the secrets of human magic stored in the Great Sorcerer\'s amulet for yourself.',
    special: 'Restore one Health after killing a Monster',
    condition: 'Die to a monster'
	}),
  SCAVENGER: Object.freeze({
    id: 0b10000000000,
    hidden: true,
    name: 'Scavenger',
    description: 'A lowlife from the City left with no other choice but to enter The Ruin in search of a few coins.',
    special: 'Obtain one additional Gold from all sources',
    condition: 'Beat the game with the Scavenger status.'
	}),
  COWARD: Object.freeze({
    id: 0b100000000000,
    hidden: false,
    name: 'Coward',
    description: 'A sniveling wretch forced into The Ruin by circumstance and desperate to leave.',
    special: 'Can use Teleport without cost',
    condition: 'Escape the dungeon in the first room with zero gold.'
	}),
  PRIEST: Object.freeze({
    id: 0b1000000000000,
    hidden: false,
    name: 'Priest',
    description: 'A member of the clergy hoping to purify the evil magics that raise those who die in The Ruin.',
    special: 'Restore heals all missing Health',
    condition: 'Heal yourself from one Health to six.'
	}),
  BARBARIAN: Object.freeze({
    id: 0b10000000000000,
    hidden: false,
    name: 'Barbarian',
    description: 'A valiant warrior from the Far North here to train their courage in The Ruin.',
    special: 'Monsters can be played on Chests and Traps to resolve them',
    condition: 'Dismantle a Trap and a Chest on the same turn.'
	}),
  SPELLBLADE: Object.freeze({
    id: 0b100000000000000,
    hidden: false,
    name: 'Spellblade',
    description: 'An adventurer that walks down both the path of the blade and that of the arcane arts.',
    special: 'Weapons can be used in place of Runes to cast spells',
    condition: 'Kill four Monsters with Blast in a single turn.'
	}),
	NECROMANCER: Object.freeze({
    id: 0b1000000000000000,
    hidden: true,
    name: 'Necromancer',
    description: 'An vile and evil mage hoping to learn secrets hidden in The Ruins ability to raise the dead.',
    special: 'Monsters become Weapons when killed',
    condition: 'Die to a Monster after your Weapon shatters.'
	})
});
