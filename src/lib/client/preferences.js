import { writable } from 'svelte/store';

function getThemePreference() {
	return window.matchMedia('(prefers-color-scheme: dark)').matches
		? 'dark'
		: 'light';
}

function loadTheme() {
	if (typeof window !== 'undefined') {
		const { subscribe, set, update } = writable(
			localStorage.getItem('theme') || getThemePreference()
		);

		return {
			subscribe,
			toggle: () =>
				update(n => {
					let theme = n === 'dark' ? 'light' : 'dark';

					localStorage.setItem('theme', theme);

					return theme;
				}),
			reset: () => {
				localStorage.removeItem('theme');
				set(getThemePreference());
			}
		};
	}
}

function getMotionPreference() {
	return !window.matchMedia('(prefers-reduced-motion: reduce)').matches;
}

function loadMotion() {
	if (typeof window !== 'undefined') {
		const { subscribe, set, update } = writable(
			localStorage.getItem('motion') || getMotionPreference()
		);

		return {
			subscribe,
			toggle: () =>
				update(n => {
					localStorage.setItem('motion', !n);

					return !n;
				}),
			reset: () => {
				localStorage.removeItem('motion');
				set(getMotionPreference());
			}
		};
	}
}

export const theme = loadTheme();
export const motion = loadMotion();
