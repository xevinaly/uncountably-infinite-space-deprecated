import { writable } from 'svelte/store';

const quantity = 4;

let progress = 0;
let data = [];

function getRings() {
	return data.map(ring => {
		return { cx: ring.cx, cy: ring.cy };
	});
}

function loadRings() {
	for (let i = 0; i < quantity; i++) {
		data.push({
			cx: 50,
			cy: 50,
			radius: Math.random() * 9,
			time: Math.random() * 2 * Math.PI,
			direction: Math.random() > 0.5 ? -Math.random() : Math.random()
		});
	}

	progress = 0.0;

	setTimeout(() => {
		setTimeout(moveToPoint, 17);
	}, 1000);

	const { subscribe, set } = writable(getRings());

	return {
		subscribe,
		update: () => set(getRings())
	};
}

const rings = loadRings();

function moveToPoint() {
	progress = Math.min(1, progress + 1 / 100);

	data.forEach(ring => {
		ring.cx = easeOutQuad(progress, 50, ring.radius * Math.cos(ring.time));
		ring.cy = easeOutQuad(progress, 50, ring.radius * Math.sin(ring.time));
	});

	rings.update();

	if (progress >= 1) {
		progress = Math.PI / 60;
		setTimeout(spin, 17);
	} else setTimeout(moveToPoint, 17);
}

function spin() {
	progress += Math.PI / 60;

	data.forEach(ring => {
		ring.cx =
			(ring.radius + Math.cos(progress * ring.direction)) *
				Math.cos(ring.time + progress * ring.direction) +
			50;
		ring.cy =
			(ring.radius + Math.cos(progress * ring.direction)) *
				Math.sin(ring.time + progress * ring.direction) +
			50;
	});

	rings.update();

	setTimeout(spin, 17);
}

function easeOutQuad(elapsed, start, difference) {
	return -difference * (elapsed /= 1) * (elapsed - 2) + start;
}

export { rings };
