import { validate } from '$lib/server/games/quiddler/users.js';
import { validateOwner } from '$lib/server/games/quiddler/hub.js';

const games = new Map();

games.set('quiddler', {
	validateUser: validate,
	validateOwner
});

export function validateUserType(data) {
	if (
		!games.has(data.game) ||
		!games.get(data.game).validateUser(data.username, data.token)
	)
		return null;
	if (!games.get(data.game).validateOwner(data.id, data.username))
		return 'user';
	return 'owner';
}
