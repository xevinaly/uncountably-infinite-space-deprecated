import seedrandom from 'seedrandom';
import WebSocket from 'isomorphic-ws';
import { validateUserType } from './validation.js';

const connections = new Map();
const hosts = new Map();
const server = new WebSocket.Server({ port: 8181 });

server.on('connection', client => {
	const key = generateClientKey();

	connections.set(key, client);

	client.on('message', response => {
		if (response == 'Heartbeat') return;

		const message = JSON.parse(response);
		const data = message.data;

		if (message.method == 'host')
			if (validateUserType(data) == 'owner')
				hosts.set(`${data.game}-${data.id}`, key);
			else client.close();
		else if (message.method == 'join')
			if (
				hosts.has(`${data.game}-${data.id}`) &&
				validateUserType(data) == 'user'
			)
				client.send(
					JSON.stringify({
						method: 'join',
						data: {
							connection: hosts.get(`${data.game}-${data.id}`)
						}
					})
				);
			else client.close();
		else if (message.method == 'offer')
			if (connections.has(data.connection))
				connections.get(data.connection).send(
					JSON.stringify({
						method: 'offer',
						data: {
							connection: key,
							description: data.description
						}
					})
				);
			else client.close();
		else if (message.method == 'answer')
			if (connections.has(data.connection)) {
				connections.get(data.connection).send(
					JSON.stringify({
						method: 'answer',
						data: {
							description: data.description
						}
					})
				);
			} else client.close();
		else if (message.method == 'candidate')
			try {
				if (connections.has(data.connection))
					connections.get(data.connection).send(
						JSON.stringify({
							method: 'candidate',
							data: {
								connection: key,
								candidate: data.candidate
							}
						})
					);
				else client.close();
			} catch (err) {
				console.log(err);
			}
		else client.close();
	});
});

function generateClientKey() {
	const random = seedrandom();
	let key;

	do {
		key = random();
	} while (connections.has(key));

	return key;
}

export function startWebsocket() {}
