import { generateID, lobbies } from './util.js';
import { broadcastLobby } from './websocket.js';

export function addLobby(data) {
	let lobby = {
		id: generateID(),
		owner: data.username,
		name: data.name,
		password: data.password,
		max_players: data.max_players,
		active: false,
		private: data.password ? true : false,
		players: [data.username],
		spectators: [],
		banned: []
	};

	lobbies.push(lobby);

	broadcastLobby(lobby);

	return lobby.id;
}

export function removeLobby(id) {
	const lobby = lobbies.filter(lobby => lobby.id == id)[0];

	broadcastLobby(lobby, 'close');

	lobbies.splice(lobbies.indexOf(lobby), 1);
}

export function removeUserFromLobbies(username) {
	let emptyLobbies = [];

	lobbies.forEach(lobby => {
		lobby.players = lobby.players.filter(player => player != username);

		if (lobby.players.length == 0) emptyLobbies = [...emptyLobbies, lobby];
	});

	emptyLobbies.forEach(lobby => {
		broadcastLobby(lobby, 'close');
		lobbies.splice(lobbies.indexOf(lobby), 1);
	});
}

export function getLobbyByID(id) {
	return lobbies.filter(lobby => lobby.id == id)[0];
}

export function validateOwner(id, username) {
	return (
		lobbies.filter(lobby => lobby.id == id && lobby.owner == username).length ==
		1
	);
}

export function reset(id) {
	lobbies.forEach(lobby => {
		if (lobby.id == id) {
			if (!lobby.active) lobby.players = [lobby.owner];
			lobby.spectators = [];

			broadcastLobby(lobby, 'update');
		}
	});
}

export function removeUser(id, username) {
	lobbies.forEach(lobby => {
		if (lobby.id == id) {
			lobby.players = lobby.players.filter(player => player != username);
			lobby.spectators = lobby.spectators.filter(
				spectator => spectator != username
			);

			broadcastLobby(lobby, 'update');
		}
	});
}

export function addSpectator(id, username) {
	lobbies.forEach(lobby => {
		if (lobby.id == id && !lobby.spectators.includes(username)) {
			lobby.spectators = [...lobby.spectators, username];

			broadcastLobby(lobby, 'update');
		}
	});
}

export function removeSpectator(id, username) {
	lobbies.forEach(lobby => {
		if (lobby.id == id) {
			lobby.spectators = lobby.spectators.filter(
				spectator => spectator != username
			);

			broadcastLobby(lobby, 'update');
		}
	});
}

export function addPlayer(id, username) {
	lobbies.forEach(lobby => {
		if (lobby.id == id && !lobby.players.includes(username)) {
			lobby.players = [...lobby.players, username];
			lobby.spectators = lobby.spectators.filter(
				spectator => spectator != username
			);

			broadcastLobby(lobby, 'update');
		}
	});
}

export function removePlayer(id, username) {
	lobbies.forEach(lobby => {
		if (lobby.id == id) {
			lobby.players = lobby.players.filter(player => player != username);
			if (!lobby.spectators.includes(username))
				lobby.spectators = [...lobby.spectators, username];

			broadcastLobby(lobby, 'update');
		}
	});
}

export function banUser(id, username) {
	lobbies.forEach(lobby => {
		if (lobby.id == id && !lobby.banned.includes(username)) {
			lobby.banned = [...lobby.banned, username];
			lobby.players = lobby.players.filter(player => player != username);
			lobby.spectators = lobby.spectators.filter(
				spectator => spectator != username
			);

			broadcastLobby(lobby, 'update');
		}
	});
}

export function unbanUser(id, username) {
	lobbies.forEach(lobby => {
		if (lobby.id == id) {
			lobby.banned = lobby.banned.filter(user => user != username);

			broadcastLobby(lobby, 'update');
		}
	});
}

export function startGame(id) {
	lobbies.forEach(lobby => {
		if (lobby.id == id) {
			lobby.active = true;
			broadcastLobby(lobby, 'update');
		}
	});
}

export function endGame(id) {
	lobbies.forEach(lobby => {
		if (lobby.id == id) {
			lobby.active = false;
			broadcastLobby(lobby, 'update');
		}
	});
}
