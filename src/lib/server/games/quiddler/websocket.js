import WebSocket from 'isomorphic-ws';
import { lobbies } from './util.js';

const server = new WebSocket.Server({ port: 8080 });

server.on('connection', client => {
	client.send(
		JSON.stringify({
			method: 'connect',
			data: lobbies.map(lobby => removeSensistiveData(lobby))
		})
	);
});

export function broadcastLobby(lobby, method = 'open') {
	server.clients.forEach(client => {
		client.send(
			JSON.stringify({
				method,
				data: removeSensistiveData(lobby)
			})
		);
	});
}

function removeSensistiveData(lobby) {
	let { password, ...clean } = lobby;
	return {
		...clean,
		password: ''
	};
}

export function startWebsocket() {}
