import { removeUserFromLobbies } from '$lib/server/games/quiddler/hub.js';
import { TIME, HTTP } from '$lib/constants.js';

export let users = [];

export function addUser(username, token) {
	if (users.filter(user => user.username == username).length != 0)
		return HTTP.CONFLICT;

	users.push({
		username,
		token,
		expiration: setTimeout(() => {
			removeUser(username, token);
		}, TIME.TEN_MINUTES)
	});

	return HTTP.CREATED;
}

export function heartbeat(username, token) {
	let user = users.filter(user => user.username == username)[0];

	if (!user) return HTTP.GONE;
	if (user.token != token) return HTTP.FORBIDDEN;

	clearTimeout(user.expiration);
	user.expiration = setTimeout(() => {
		removeUser(username, token);
	}, TIME.TEN_MINUTES);

	return HTTP.NO_CONTENT;
}

export function validate(username, token) {
	let user = users.filter(user => user.username == username)[0];

	if (!user) return HTTP.GONE;
	if (user.token != token) return HTTP.FORBIDDEN;

	return HTTP.OK;
}

export function removeUser(username, token) {
	let user = users.filter(user => user.username == username)[0];

	if (!user) return HTTP.GONE;
	if (user.token != token) return HTTP.FORBIDDEN;

	clearTimeout(user.expiration);

	removeUserFromLobbies(username);

	users = users.filter(user => user.username != username);

	return HTTP.NO_CONTENT;
}
