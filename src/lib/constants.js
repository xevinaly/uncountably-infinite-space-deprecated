export const WEBSOCKET = Object.freeze({
	NORMAL_CLOSURE_CODE: 1000
});

export const TIME = Object.freeze({
	FIVE_SECONDS: 5000,
	TWENTY_SECONDS: 20000,
	ONE_MINUTE: 60000,
	TEN_MINUTES: 600000
});

export const HTTP = Object.freeze({
	OK: 200,
	CREATED: 201,
	NO_CONTENT: 204,
	STATUS_ERROR_THRESHOLD: 300,
	BAD_REQUEST: 400,
	FORBIDDEN: 403,
	NOT_FOUND: 404,
	CONFLICT: 409,
	GONE: 410,
	LOCKED: 423
});

export const configuration = Object.freeze({
	iceServers: [
		{
			urls: 'stun:stun.12connect.com:3478'
		},
		{
			urls: 'stun:stun1.l.google.com:19302'
		},
		{
			urls: 'stun:stun2.l.google.com:19302'
		},
		{
			urls: 'stun:stun3.l.google.com:19302'
		},
		{
			urls: 'stun:stun4.l.google.com:19302'
		},
		{
			urls: 'stun:stun.uncountablyinfinite.space:5349'
		},
		{
			urls: 'turn:turn.uncountablyinfinite.space:5349',
			username: 'quiddler',
			credential: 'th7t3h7'
		},
		{
			urls: 'stun:stun.104.131.173.183:5349'
		},
		{
			urls: 'turn:turn.104.131.173.183:5349',
			username: 'quiddler',
			credential: 'th7t3h7'
		}
	]
});
