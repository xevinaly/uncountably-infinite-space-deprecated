import { build, files, timestamp } from '$service-worker';

const version = timestamp;

self.addEventListener('install', event => {
	event.waitUntil(
		caches
			.open(version)
			.then(cache => cache.addAll(build.concat(files)))
			.then(self.skipWaiting())
	);
});

self.addEventListener('activate', event => {
	event.waitUntil(
		caches.keys().then(keys => {
			return Promise.all(
				keys.filter(key => key != version).map(key => caches.delete(key))
			);
		})
	);
});

self.addEventListener('fetch', event => {
	if (event.request.url.startsWith(self.location.origin)) {
		if (event.request.method !== 'GET') return;

		event.respondWith(
			caches
				.match(event.request)
				.then(
					cached =>
						cached ||
						caches
							.open(version)
							.then(cache =>
								fetch(event.request).then(response =>
									cache
										.put(event.request, response.clone())
										.then(() => response)
								)
							)
				)
		);
	}
});
