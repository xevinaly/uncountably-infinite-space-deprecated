# Uncountably Infinite Space

Last year, I read a blog post by an old friend that set me on the path to creating my own website. I've had the skills for a basic website for years, but had never committed to creating my own. I'd never known what to put, or what to use. This is an attempt to get myself on track with that.

## Contributing

This is going to be a personal project, but I wanted to make it available to anyone that wanted to view the source for it, either for their own site, or just as a reference.

I'm using SvelteKit as my dev environment/framework and have a [live site](http://uncountablyinfinite.space/) deployed to Digital Ocean.

## Developing

```bash
yarn run dev

# or start the server and open the app in a new browser tab
yarn run dev -- --open
```

## Building

```bash
yarn run build
```

> You can preview the built app with `yarn run preview`, regardless of whether you installed an adapter. This should _not_ be used to serve your app in production.
