import { defineConfig } from 'windicss/helpers';
import plugin from 'windicss/plugin';

export default defineConfig({
	darkMode: 'class',
	attributify: false,
	theme: {
		extend: {
			colors: {
				grey: '#EEEBE8',
				smoke: '#F3F0ED',
				flare: '#FBF8F5',
				ruby: '#FF3E00',
				gray: '#303030',
				soot: '#2B2B2B',
				eclipse: '#232323',
				terminal: '#39FF14',
				horizon: 'rgba(251,114,0,0.8)',
				stop: '#B81D13',
				yield: '#EFB700',
				go: '#008450'
			},
			fontFamily: {
				source: ['Source Code Pro', 'sans-serif']
			},
			screens: {
				mobileS: '320px',
				mobile: '375px',
				mobileL: '425px',
				tabletS: '600px',
				tablet: '768px',
				laptop: '1024px',
				laptopL: '1440px',
				fourK: '2200px'
			},
			width: {
				fit: 'fit-content',
				padded: 'calc(100% - 1rem)'
			}
		}
	},
	alias: {
		'heading-1': 'text-5xl mobileL:text-6xl tabletS:text-7xl tablet:text-8xl'
	},
	plugins: [
		plugin(({ addUtilities }) => {
			const newUtilities = {
				'.scrollbar-hidden': {
					'-ms-overflow-style': 'none',
					'scrollbar-width': 'none',
					'&::-webkit-scrollbar': {
						display: 'none'
					}
				},
				'.shadow-clone': {
					'-webkit-box-shadow': '0.2em 0.2em 0 0 var(--tw-shadow-color)',
					'box-shadow': '0.2em 0.2em 0 0 var(--tw-shadow-color)'
				},
				'.shadow-clone-sm': {
					'-webkit-box-shadow': '0.1em 0.1em 0 0 var(--tw-shadow-color)',
					'box-shadow': '0.1em 0.1em 0 0 var(--tw-shadow-color)'
				},
				'.shadow-meter': {
					'-webkit-box-shadow': '-50.5rem 0 0 50rem var(--tw-shadow-color)',
					'box-shadow': '-50.5rem 0 0 50rem var(--tw-shadow-color)'
				}
			};
			addUtilities(newUtilities);
		})
	]
});
