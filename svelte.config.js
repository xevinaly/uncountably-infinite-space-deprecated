import windi from 'vite-plugin-windicss';
import node from '@sveltejs/adapter-node';

const config = {
	kit: {
		adapter: node(),
		csp: {
      directives: {
        'script-src': ['self']
      }
    },
		vite: () => ({
			plugins: [
				windi({
					configPath: 'windi.config.js'
				})
			]
		})
	}
};

export default config;
